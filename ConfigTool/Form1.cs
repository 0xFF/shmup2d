﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ConfigTool
{
    public partial class ConfigToolWindows : Form
    {
        public ConfigToolWindows()
        {
            InitializeComponent();
        }

        private void widthTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void widthTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void heightTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            XMLConfig xConf = new XMLConfig("config.xml",true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
