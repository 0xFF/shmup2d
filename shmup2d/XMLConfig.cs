﻿using System;
using System.Text;
using System.IO;
using System.Xml;

namespace Shmup2d
{
    class XMLConfig
    {
        public string FileName;
        private FileStream fs;
        private XmlDocument xmlDoc;

        XMLConfig(String fileName, bool Create)
        {

            this.FileName = fileName;

            if (File.Exists(fileName))
            {
                fs = new FileStream(FileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                xmlDoc = new XmlDocument();
                xmlDoc.Load(fs);
            }
            else if (Create)
            {
                fs = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(@"<config>
                                    <screen width=""1200"" height=""750""/>
                                </config>");
                xmlDoc.Save(fs);
            }
            else
            {
                Exception up = new Exception("Specified file not found.");
                throw up;
            }
        }

        public bool saveScreenConfig(int width, int height)
        {
            //code here
            return true;

        }

        ~XMLConfig()
        {
            fs.Close();
        }
    }
}
