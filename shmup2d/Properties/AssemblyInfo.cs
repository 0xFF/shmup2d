﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Shmup2d")]
[assembly: AssemblyProduct("Shmup2d")]
[assembly: AssemblyDescription("2D Vertical Scrolling Shmup")]
[assembly: AssemblyCompany("Nacereddine HOURIA")]

[assembly: AssemblyCopyright("Copyright © 2009, HOURIA Nacereddine.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("41b8e9fe-21d5-4c72-b758-884c52562446")]


// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
