﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Shmup2d
{
    class PlayerShip:Ship
    {

        public int Score;

        public PlayerShip(int screenWidth, int screenHeight, string assetName)
            : base(screenWidth, screenHeight, assetName)
        {
            ScreenWidth = screenWidth;
            ScreenHeight = screenHeight;
            AssetName = assetName;
        }

        public void Move(int direction, GameTime gameTime)
        {
            switch (direction)
            {
                case 0:
                    if (Position.Y > 0)
                    {
                        Position.Y -= ShipVelocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    }
                    break;                     
                case 1:
                    if (Position.X + Size.Width < ScreenWidth )
                    {
                        Position.X += ShipVelocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    }
                    break;
                case 2:
                    if (Position.Y + Size.Height < ScreenHeight)
                    {
                        Position.Y += ShipVelocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    }
                    
                    break;
                case 3:
                    if (Position.X > 0)
                    {
                        Position.X -= ShipVelocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    }
                    
                    break;
            }
        }

        public void Update(GameTime gameTime, Sprite enemy, List<AnimatedSprite> explosions)
        {
            base.Update(gameTime,enemy,explosions);

            foreach (Bullet bullet in Gunfire1)
            {
                if (Collisions.Collides(bullet, enemy) && bullet.Visible)
                {
                    Score += 10;
                    bullet.Visible = false;
                    AnimatedSprite aSprite = new AnimatedSprite((int)(bullet.center().X - 150),
                                                                (int)(bullet.center().Y - 150),
                                                                @"Sprites\Explosion\explosion", 53);
                    explosions.Add(aSprite);

                }
            }

            foreach (Bullet bullet in Gunfire2)
            {
                if (Collisions.Collides(bullet, enemy) && bullet.Visible)
                {
                    Score += 10;
                    bullet.Visible = false;
                    AnimatedSprite aSprite = new AnimatedSprite((int)(bullet.center().X - 150),
                                                                (int)(bullet.center().Y - 150),
                                                                @"Sprites\Explosion\explosion", 53);
                    explosions.Add(aSprite);

                }
            }

            
        }

    }

}
