﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


namespace Shmup2d
{
    class Bullet:Sprite
    {
        public bool Visible = true;

        public Bullet(string assetName)
        {
            AssetName = assetName;
        }

        public void LoadContent(ContentManager theContentManager)
        {
            Texture = theContentManager.Load<Texture2D>(AssetName);
            Size = new Rectangle(0, 0, (int)(Texture.Width * Scale), (int)(Texture.Height * Scale));
        }

    }
}
