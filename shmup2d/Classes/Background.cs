﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Shmup2d
{
    class Background
    {
        private int ScreenWidth;
        private int ScreenHeight;
        private string AssetName;

        public Sprite MBackground1;
        public Sprite MBackground2;
        public Vector2 Velocity = new Vector2(0, 160);
        public Vector2 Direction = new Vector2(0, 1);
        

        public Background(int screenWidth, int screenHeight, string assetName)
        {
            ScreenWidth = screenWidth;
            ScreenHeight = screenHeight;
            AssetName = assetName;
        }
        


        public void LoadContent(ContentManager contentManger){
            MBackground1 = new Sprite(ScreenWidth, ScreenHeight, AssetName, Vector2.Zero);
            MBackground2 = new Sprite(ScreenWidth, ScreenHeight, AssetName, new Vector2(0,ScreenHeight));

            MBackground1.LoadContent(contentManger);
            MBackground2.LoadContent(contentManger);
        }

        public void Update(GameTime gameTime){
            if (MBackground1.Position.Y > MBackground1.Size.Height)
            {
                MBackground1.Position = new Vector2(MBackground1.Position.X,
                                                    MBackground2.Position.Y - MBackground2.Size.Height);
            }

            if (MBackground2.Position.Y > MBackground2.Size.Height)
            {
                MBackground2.Position = new Vector2(MBackground2.Position.X,
                                                    MBackground1.Position.Y - MBackground1.Size.Height);
            }

            MBackground1.Position += Direction * Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
            MBackground2.Position += Direction * Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        public void Draw(SpriteBatch spriteBatch){
            MBackground1.Draw(spriteBatch);
            MBackground2.Draw(spriteBatch);
        }

    }
}
