﻿using System;

namespace Shmup2d
{
    class Collisions
    {
        public static bool Collides(Sprite sprite1, Sprite sprite2)
        {
            if ((Math.Abs(sprite1.center().X - sprite2.center().X) < ((sprite1.Texture.Width / 2) + (sprite2.Texture.Width / 2))) &&
                (Math.Abs(sprite1.center().Y - sprite2.center().Y) < ((sprite1.Texture.Height / 2) + (sprite2.Texture.Height / 2))))
            {
                return true;
            }
            return false;
        }

        public static bool Collides(Sprite sprite1, Sprite sprite2,float approximation)
        {
            float sprite1Width = (approximation * sprite1.Size.Width) / 100;
            float sprite2Width = (approximation * sprite2.Size.Width) / 100;
            float sprite1Height = (approximation * sprite1.Size.Width) / 100;
            float sprite2Height = (approximation * sprite2.Size.Width) / 100;

            if ((Math.Abs(sprite1.center().X - sprite2.center().X) < (sprite1Width / 2) + (sprite2Width / 2)) &&
                (Math.Abs(sprite1.center().Y - sprite2.center().Y) < (sprite1Height / 2) + (sprite2Height / 2)))
            {
                return true;
            }
            return false;
        }
    }
}
