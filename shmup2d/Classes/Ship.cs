﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Shmup2d
{
    class Ship : Sprite
    {

        protected double TimeSinceFired;
        protected int ShipVelocity = 500;
        protected int BulletsVelocity = 15;

        public int Health = 100;
        public List<Bullet> Gunfire1 = new List<Bullet>();
        public List<Bullet> Gunfire2 = new List<Bullet>();

        public Ship(int screenWidth, int screenHeight, string assetName)
        {
            ScreenWidth = screenWidth;
            ScreenHeight = screenHeight;
            AssetName = assetName;
        }


        public void Update(GameTime gameTime, Sprite enemy, List<AnimatedSprite> explosions)
        {
            for (int i = 0; i < Gunfire1.Count; i++)
            {
                if (Gunfire1[i].Position.X == -5 || !Gunfire1[i].Visible)
                {
                    Gunfire1.RemoveAt(i);
                    i--;
                }
                else
                {
                    Gunfire1[i].Position.Y -= BulletsVelocity;
                    if (Gunfire1[i].Position.Y < -Gunfire1[i].Size.Height)
                    {
                        Gunfire1.RemoveAt(i);
                        i--;
                    }
                }
            }
            for (int i = 0; i < Gunfire2.Count; i++)
            {
                if (Gunfire2[i].Position.X == -5 || !Gunfire2[i].Visible)
                {
                    Gunfire2.RemoveAt(i);
                    i--;
                }
                else
                {
                    Gunfire2[i].Position.Y -= BulletsVelocity;
                    if (Gunfire2[i].Position.Y < -Gunfire2[i].Size.Height)
                    {
                        Gunfire2.RemoveAt(i);
                        i--;
                    }
                }
            }

        }

        public override void LoadContent(ContentManager theContentManager)
        {
            base.LoadContent(theContentManager);

            Position = new Vector2((ScreenWidth / 2) - (Size.Width / 2),
                                    ScreenHeight - Size.Height);
        }

        public void Fire(ContentManager theContentManager, GameTime gameTime)
        {
            if (gameTime.TotalGameTime.TotalMilliseconds - TimeSinceFired > 100)
            {
                Bullet aBullet1 = new Bullet(@"Sprites\Shots\black");
                Bullet aBullet2 = new Bullet(@"Sprites\Shots\black");

                aBullet1.LoadContent(theContentManager);
                aBullet2.LoadContent(theContentManager);

                aBullet1.Position = new Vector2(center().X - 30 - (aBullet1.Size.Width / 2),
                                                Position.Y + (aBullet1.Size.Height * 1.5f));

                aBullet2.Position = new Vector2(center().X + 30 - (aBullet2.Size.Width / 2),
                                                Position.Y + (aBullet2.Size.Height * 1.5f));

                Gunfire1.Add(aBullet1);
                Gunfire2.Add(aBullet2);

                TimeSinceFired = gameTime.TotalGameTime.TotalMilliseconds;
            }

        }



        public void Draw(SpriteBatch spriteBatch)
        {

            foreach (Bullet bullet1 in Gunfire1)
            {
                if (bullet1.Visible)
                    bullet1.Draw(spriteBatch);
            }
            foreach (Bullet bullet2 in Gunfire2)
            {
                if (bullet2.Visible)
                    bullet2.Draw(spriteBatch);
            }
            base.Draw(spriteBatch);

        }
    }

}
