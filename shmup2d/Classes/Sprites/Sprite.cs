﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Shmup2d
{
    class Sprite
    {
        public string AssetName;
        public Vector2 Position;
        public Rectangle Size;
        public Texture2D Texture;
        public float Scale = 1.0f;
        protected int ScreenWidth = 0;
        protected int ScreenHeight = 0;

        public Sprite()
        {
            Position = Vector2.Zero;
        }

        public Sprite(int screenWidth, int screenHeight)
        {
            ScreenWidth = screenWidth;
            ScreenHeight = screenHeight;
            Position = Vector2.Zero;
        }

        public Sprite(int screenWidth, int screenHeight, string assetName){
            ScreenWidth = screenWidth;
            ScreenHeight = screenHeight;
            AssetName = assetName;
            Position = Vector2.Zero;
        }

        public Sprite(int screenWidth, int screenHeight , string assetName, Vector2 position)
        {
            ScreenWidth = screenWidth;
            ScreenHeight = screenHeight;
            AssetName = assetName;
            Position = position;
        }

        public virtual void LoadContent(ContentManager theContentManager)
        {
            Texture = theContentManager.Load<Texture2D>(AssetName);
            Size = new Rectangle(0, 0, (int)(Texture.Width * Scale), (int)(Texture.Height * Scale));
        }

        public void Draw(SpriteBatch theSpriteBatch)
        {
            theSpriteBatch.Draw(Texture, 
                                Position,
                                new Rectangle(0, 0, Texture.Width, Texture.Height),
                                Color.White, 
                                0.0f, 
                                Vector2.Zero, 
                                Scale, 
                                SpriteEffects.None, 0);
            
        }

        public void Draw(SpriteBatch theSpriteBatch, Color color)
        {
            theSpriteBatch.Draw(Texture,
                                Position,
                                new Rectangle(0, 0, Texture.Width, Texture.Height),
                                color, 0.0f,
                                Vector2.Zero,
                                Scale,
                                SpriteEffects.None, 0);
        }

        public void Draw(SpriteBatch theSpriteBatch, Color color, SpriteEffects effect)
        {
            theSpriteBatch.Draw(Texture,
                                Position,
                                new Rectangle(0, 0, Texture.Width, Texture.Height),
                                color, 0.0f,
                                Vector2.Zero,
                                Scale,
                                effect, 0);
        }

        public Vector2 center(){

            return new Vector2(Position.X + (Size.Width / 2), 
                                Position.Y + (Size.Height / 2));

        }
        
    }
}
