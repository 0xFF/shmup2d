﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


namespace Shmup2d
{
    class AnimatedSprite:Sprite
    {
        public string AssetName = "";
        public int CurrentFrameNumber;
        public int TotalFramesCount;
        public bool AnimationOver;

        public AnimatedSprite(int x, int y, string asset, int frameCount){
            AssetName = asset;
            TotalFramesCount = frameCount;
            Position.X = x;
            Position.Y = y;
        }

        public virtual void LoadContent(ContentManager theContentManager)
        {
            Texture = theContentManager.Load<Texture2D>(AssetName + CurrentFrameNumber);
            Size = new Rectangle(0, 0, (int)(Texture.Width * Scale), (int)(Texture.Height * Scale));
        }

        public void Update(ContentManager theContentManager)
        {
            if (CurrentFrameNumber <= TotalFramesCount)
            {
                LoadContent(theContentManager);
                CurrentFrameNumber++;
            }
            else {
                AnimationOver = true;
            }
        }


    }
}
