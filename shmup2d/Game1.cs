using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Shmup2d
{
    public struct ShowText 
    {
        public string textToShow;
        public int timeToShow;

    }

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        const int ScreenWidth = 1200;
        const int ScreenHeight = 750;
        const bool FullScreen = false;
        const string WindowTitle = "PROJECT SHMUP 2D :-) (TESTING)";
        
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        PlayerShip playerShip;
        Background background;
        SpriteFont debugFont;
        SpriteFont gameFont;
        Sprite Enemy;
        ParticleEngine particleEngine;
        
        public ShowText txt;

        string PlaneName = "Su-47 Berkut";
        int gameFPS;

        List<AnimatedSprite> explosions = new List<AnimatedSprite>();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            background = new Background(ScreenWidth,ScreenHeight,@"Sprites\background");
            playerShip = new PlayerShip(ScreenWidth, ScreenHeight, @"Sprites\Ships\SU-47");
            Enemy = new Sprite(ScreenWidth,ScreenHeight,@"Sprites\Ships\SU-47", new Vector2(550, 50));
            
            txt.textToShow = "Game Starts";
            txt.timeToShow = 6000;

            base.Initialize();
            graphics.PreferredBackBufferWidth = ScreenWidth;
            graphics.PreferredBackBufferHeight = ScreenHeight;
            graphics.IsFullScreen = FullScreen ;
            graphics.ApplyChanges();
            Window.Title = WindowTitle;

            
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            playerShip.LoadContent(Content);
            background.LoadContent(Content);
            Enemy.LoadContent(Content);
            
            //spriteFont = Content.Load<SpriteFont>(@"Texture");
            debugFont = Content.Load<SpriteFont>(@"Sprites\debug");
            gameFont = Content.Load<SpriteFont>(@"Sprites\sovietFont");
            List<Texture2D> textures = new List<Texture2D>
                                           {
                                               Content.Load<Texture2D>(@"Particles\circle-big")
                                           };
            particleEngine = new ParticleEngine(textures, new Vector2(400, 300));

            
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here

        }
        
        protected override void Update(GameTime gameTime)
        {
            /*
            Enemy.Position.X = Mouse.GetState().X;
            Enemy.Position.Y = Mouse.GetState().Y;
            */
            txt.timeToShow--;
            gameFPS = (int)(1 / gameTime.ElapsedGameTime.TotalSeconds);
            #region moving the ship
            if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.Z))
            {
                playerShip.Move(0, gameTime);
            }
            if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.D))
            {
                playerShip.Move(1, gameTime);
            }
            if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.S))
            {
                playerShip.Move(2, gameTime);
            }
            if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.Q))
            {
                playerShip.Move(3, gameTime);
            }

            playerShip.Position.X = Mouse.GetState().X;
            playerShip.Position.Y = Mouse.GetState().Y;
            #endregion

            background.Update(gameTime);
            playerShip.Update(gameTime, Enemy, explosions);

            particleEngine.EmitterLocation = new Vector2(playerShip.center().X, playerShip.Position.Y + playerShip.Size.Height);
            particleEngine.Update();

            if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.Space) || (Mouse.GetState().LeftButton == ButtonState.Pressed))
            {
                playerShip.Fire(Content,gameTime);
            }

            for (int i = 0; i < explosions.Count; i++)
            {
                if (!explosions[i].AnimationOver)
                {
                    explosions[i].Update(Content);
                }
                else
                {
                    explosions.RemoveAt(i);
                    i--;
                }
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            spriteBatch.Begin();
            background.Draw(spriteBatch);
            spriteBatch.End();
            spriteBatch.Begin(SpriteBlendMode.Additive);
            particleEngine.Draw(spriteBatch);
            spriteBatch.End();
            spriteBatch.Begin();
            playerShip.Draw(spriteBatch);
            if (Collisions.Collides(playerShip, Enemy))
            {
                Enemy.Draw(spriteBatch, Color.Red, SpriteEffects.FlipVertically);
            }
            else
            {
                Enemy.Draw(spriteBatch, Color.White, SpriteEffects.FlipVertically);
            }

            //Enemy.Draw(spriteBatch, Color.White, SpriteEffects.FlipVertically);

            foreach (AnimatedSprite explosion in explosions)
            {
                if (!explosion.AnimationOver)
                {
                    explosion.Draw(spriteBatch);
                }
            }
            // ---- Debug Info Begin ----
            spriteBatch.DrawString(debugFont,
                                    "Current FPS     " + gameFPS + "\n" +
                                    "Current Plane   " + PlaneName + "\n" +
                                    "Player Position " + playerShip.Position + "\n" +
                                    "Player Center   " + playerShip.center() + "\n" +
                                    "Player Size     " + playerShip.Size + "\n" +
                                    "Enemy Position  " + Enemy.Position + "\n" +
                                    "Enemy Center    " + Enemy.center() + "\n" +
                                    "Enemy Size      " + Enemy.Size + "\n" +
                                    "Bullets 1       " + playerShip.Gunfire1.Count + "\n" +
                                    "Bullets 2       " + playerShip.Gunfire2.Count + "\n" +
                                    "Explosions      " + explosions.Count + "\n", 
                                    new Vector2(5, 5), 
                                    Color.White);
            // ---- Debug Info End----
            Vector2 ScorePos;
            ScorePos = gameFont.MeasureString("Score " + playerShip.Score);
            ScorePos = new Vector2((ScreenWidth/2)-(ScorePos.X/2),5);

            spriteBatch.DrawString(gameFont, "Score: " + playerShip.Score, ScorePos, Color.DarkRed);
            
            spriteBatch.DrawString(gameFont, txt.textToShow, new Vector2(500, 325), Color.DarkRed);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
